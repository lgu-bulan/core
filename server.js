const express = require('express');

let cors = require("cors");


const app = express();

const level1Router = require('./Routes/lvl1Access')
const level2Router = require('./Routes/lvl2Access')
const level3Router = require('./Routes/lvl3Access')


app.use(cors());
app.use('/lvl1',level1Router);  
app.use('/lvl2',level2Router);
app.use('/lvl3',level3Router);



app.get("/", (req,res)=>{
    res.send(` DICT CORE API`);
})


app.listen(3000, () => {
    console.log('Server is running on port 3000');
  });