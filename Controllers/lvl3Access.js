const config = require('../dbconfig')
const sql = require('mssql');


module.exports.getLVL3ACCESS = async (req,res) => {
    try {
        let pool = await sql.connect(config);
        let getSingleAccess = await pool.request()
        .query("SELECT * from Level3Access ");
      
        return res.json(getSingleAccess.recordsets[0])
    }
    catch (error) {
        
        console.log(error);
        return res.json(error)
    }
}

